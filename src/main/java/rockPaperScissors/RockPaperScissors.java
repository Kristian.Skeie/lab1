package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        // TODO: Implement Rock Paper Scissors
        // loop while player still wants to play
            while(true){
                String restartGame = "";
                // ask for input
                System.out.println("Let's play round " + roundCounter);
                String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
        
                // check for valid input
                while (!humanChoice.equals("paper") && !humanChoice.equals("rock") && !humanChoice.equals("scissors")){
                    System.out.println("I do not understand " + humanChoice + ". Could you try again?");
                    humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
                }
                int randInteger = (int)(Math.random()*3);
                // convert random number 0-2 to possible choices  
                String computerChoice = "";
                if(randInteger == 0){
                    computerChoice = "rock";
                }   else if(randInteger == 1){
                        computerChoice = "paper";
                    }   else {
                            computerChoice = "scissors";
                        }
            
                // Select the winner
                // if tie
                if(humanChoice.equals(computerChoice)){
                    System.out.println("Human chose "+ humanChoice + ", computer chose " + computerChoice + ". It's a tie!");
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                // if human won
                }   else if ((humanChoice.equals("rock") && computerChoice.equals("scissors")) ||
                        (humanChoice.equals("paper") && computerChoice.equals("rock")) ||
                        (humanChoice.equals("scissors") && computerChoice.equals("paper"))){
                            humanScore += 1;
                            System.out.println("Human chose "+ humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                        }
                // if computer won the game
                else {
                    computerScore += 1;
                    System.out.println("Human chose "+ humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            roundCounter += 1;
            restartGame = readInput("Do you wish to continue playing? (y/n)?");
            if (restartGame.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
        }
        
        
        
    }

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
